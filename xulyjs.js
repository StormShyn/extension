function loadXMLDoc(dir) {
  var xhttp = new XMLHttpRequest() || new ActiveXObject("MSXML2.XMLHTTP");
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("text").innerHTML += this.responseText + "\n";
    }
  };
  xhttp.open("GET", dir, true);
  xhttp.send();
}

function loadXMLDocFile(dỉr_file) {
  document.getElementById(dỉr_file).addEventListener("change", function () {
    var fr = new FileReader();
    fr.onload = function () {
      document.getElementById("text").innerHTML += fr.result + "\n";
    };

    fr.readAsText(this.files[0]);
  });
}

function buttonCopy() {
  var copyText = document.getElementById("text");

  copyText.select();
  copyText.setSelectionRange(0, 99999); // For mobile devices

  navigator.clipboard.writeText(copyText.value);
}
